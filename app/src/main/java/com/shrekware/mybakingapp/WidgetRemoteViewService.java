/*
 *
 *   PROJECT LICENSE
 *   This project was submitted by Henry Ayers as part of the Nanodegree At Udacity.
 *   As part of Udacity Honor code, your submissions must be your own work,
 *   hence submitting this project as yours will cause you to break the Udacity Honor Code
 *   and the suspension of your account. Me, the author of the project,
 *   allow you to check the code as a reference,
 *   but if you submit it, it's your own responsibility if you get expelled.
 *
 *   Copyright (c) 2018 Henry Ayers
 *
 *   Besides the above notice, the following license applies and
 *   this license notice must be included in all works derived from this project.
 *   MIT License Permission is hereby granted, free of charge,
 *   to any person obtaining a copy of this software and associated documentation files (the "Software"),
 *   to deal in the Software without restriction, including without limitation the rights to use,
 *   copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 *   and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 *   DAMAGES OR OTHER LIABILITY, WHETHER ACTION OF CONTRACT,
 *   TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package com.shrekware.mybakingapp;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;
import com.shrekware.mybakingapp.database.WidgetDatabase;
import com.shrekware.mybakingapp.database.WidgetIngredient;
import java.util.ArrayList;
import java.util.List;

public class WidgetRemoteViewService extends RemoteViewsService
{
    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent)
    {
        return new WidgetRemoteViewsFactory(this.getApplicationContext());
    }
    // Remote view class for the widget
    class WidgetRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory
    {
        private List<String> myIngredientsArray;
        private WidgetDatabase myDb;
        private Context myContext;
        private List<WidgetIngredient> myListOfIngredients;

        private WidgetRemoteViewsFactory(Context context)
        {
            myContext = context;
        }
        @Override
        public void onCreate()
        {
            Log.v("WidgetRemoteView", " ####################### onCreate  ############");
            myDb = Room.databaseBuilder(myContext,WidgetDatabase.class,"widget_ingredients").build();
        }
        @Override
        public void onDataSetChanged()
        {
          //  myIngredientsArray.clear();
            Log.v("WidgetRemoteView", " ####################### onDataSetChanged  ############");
            updateTheList();
        }
        @Override
        public void onDestroy()
        {
            Log.v("WidgetRemoteView", " ####################### onDestroy  ############");
             // myDb.clearAllTables();
        }
        @Override
        public int getCount()
        {
            Log.v("WidgetRemoteView"," ####################### getCount  ############################################################################################################################");
            Log.v("WidgetRemoteView"," ####################### getCount = "+myIngredientsArray.size());

            return  myIngredientsArray.size();
        }
        @Override
        public RemoteViews getViewAt(int position)
        {
            Log.v("WidgetRemoteView"," ####################### getViewAt "+myIngredientsArray.size());

            RemoteViews rv = new RemoteViews(myContext.getPackageName()
                    ,R.layout.widget_item);
            rv.setTextViewText(R.id.widget_list_item,myIngredientsArray.get(position));
            return rv;
        }
        @Override
        public RemoteViews getLoadingView()
        {
            return null;
        }
        @Override
        public int getViewTypeCount()
        {
            return 1;
        }
        @Override
        public long getItemId(int position)
        {
            return position;
        }
        @Override
        public boolean hasStableIds()
        {
            return true;
        }

        private void updateTheList()
        {
            if(myIngredientsArray!=null){myIngredientsArray.clear();}
            else{myIngredientsArray = new ArrayList<>();}
            Log.v("WidgetRemoteView"," ####################### update the list   ");
            Log.v("WidgetRemoteView"," ####################### update the list, myIngredientsArray size = "+myIngredientsArray.size());
            Thread thread = new Thread(() -> myListOfIngredients = myDb.widgetDao().loadAllIngredients());
            thread.start();
            while(thread.isAlive())
            if(myListOfIngredients!=null)
            {
                for (int x = 0; x < myListOfIngredients.size(); x++)
                {
                    String recipeName = myListOfIngredients.get(x).getRecipeName();
                    String ingredient = myListOfIngredients.get(x).getIngredient();
                    String measure = myListOfIngredients.get(x).getMeasure();
                    Float quantity = myListOfIngredients.get(x).getQuantity();

                    String builder = recipeName +
                            "\n" +
                            quantity +
                            " " +
                            measure +
                            " " +
                            ingredient;
                    myIngredientsArray.add(builder);
                }
            }
        }
    }
}

