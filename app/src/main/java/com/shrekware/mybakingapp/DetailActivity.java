/*
 *
 *   PROJECT LICENSE
 *   This project was submitted by Henry Ayers as part of the Nanodegree At Udacity.
 *   As part of Udacity Honor code, your submissions must be your own work,
 *   hence submitting this project as yours will cause you to break the Udacity Honor Code
 *   and the suspension of your account. Me, the author of the project,
 *   allow you to check the code as a reference,
 *   but if you submit it, it's your own responsibility if you get expelled.
 *
 *   Copyright (c) 2018 Henry Ayers
 *
 *   Besides the above notice, the following license applies and
 *   this license notice must be included in all works derived from this project.
 *   MIT License Permission is hereby granted, free of charge,
 *   to any person obtaining a copy of this software and associated documentation files (the "Software"),
 *   to deal in the Software without restriction, including without limitation the rights to use,
 *   copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 *   and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 *   DAMAGES OR OTHER LIABILITY, WHETHER ACTION OF CONTRACT,
 *   TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package com.shrekware.mybakingapp;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import com.shrekware.mybakingapp.detail.StepsListFragment;
import com.shrekware.mybakingapp.retrofit.Ingredient;
import com.shrekware.mybakingapp.retrofit.Recipe;
import com.shrekware.mybakingapp.retrofit.Step;
import com.shrekware.mybakingapp.step.StepFragment;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActivity extends AppCompatActivity implements StepFragment.OnFragmentInteractionListener
{
    // one or two pane indicator
    private boolean mTwoPane;
    // track if there was a
    private Boolean saved;
    // the step for the detail
    private Step step;
    private String frag_type;
    //holds the recipe for the detail
    private Recipe recipe;
    // holds the list of Steps for the recipe
    private List<Step> mySteps;
    // holds the list of ingredients of the recipe
    private List<Ingredient> myIngredients;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private Context myContext;
    private Intent intent;
    public DetailActivity(){ }
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_detail);
        myContext = getApplicationContext();
        // adds butterknife to this activity
        ButterKnife.bind(this);
        // checks if the layout for 2 pane is available
        if(savedInstanceState==null)
        {   saved = false;
            intent = getIntent();
            step = intent.getParcelableExtra("step");
            // get the recipe from the intent
            recipe = intent.getParcelableExtra("recipe");
            // get the list of steps for the recipe
            mySteps = intent.getParcelableArrayListExtra("theSteps");
            // gets the frag type
            frag_type=intent.getStringExtra("frag_type");
            // get the list of ingredients for the recipe
            myIngredients = intent.getParcelableArrayListExtra("theIngredients");
        }
        else
            {
                saved=true;
                frag_type = savedInstanceState.getString("frag_type");
                recipe = savedInstanceState.getParcelable("recipe");
                step = savedInstanceState.getParcelable("step");
                myIngredients = savedInstanceState.getParcelableArrayList("theIngredients");
                mySteps = savedInstanceState.getParcelableArrayList("theSteps");
            }
        if(findViewById(R.id.detail_second_frame)!=null)
        {
            mTwoPane = true;
            setDetailFragment(savedInstanceState,mTwoPane);
            Log.v("DetailActivity", "###############################  onCreate, second_frame != null, step =  "+ step.getDescription());
            showStepFragment(step,mTwoPane);
        }
        else
            {
                 mTwoPane = false;
                setDetailFragment(savedInstanceState,mTwoPane);
            }
    }
    private void setDetailFragment(Bundle savedInstanceState, Boolean twoPane)
    {
        // if the device supports 2 fragments
        StepsListFragment stepsListFragment;
        if (twoPane)
        {
            Bundle bundle = new Bundle();
            bundle.putParcelable("recipe", recipe);
            bundle.putParcelableArrayList("theIngredients", (ArrayList<? extends Parcelable>) myIngredients);
            bundle.putParcelableArrayList("theSteps", (ArrayList<? extends Parcelable>) mySteps);
            // if not being restored and is not new
           if(savedInstanceState==null & !saved)
           {
                //
                stepsListFragment = new StepsListFragment();
                stepsListFragment.setArguments(bundle);
                android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                       // .addToBackStack(null)
                        .add(R.id.detail_frame, stepsListFragment, "stepListFragment")
                        .commit();
                saved = true;
           }
        }
        // if its a single fragment showing
        else
        {
            if(frag_type==null)frag_type="stepList";
            switch (frag_type)
            {
                case "step":
                    if(savedInstanceState==null)
                    {
                        step =intent.getParcelableExtra("step");
                    }
                    else
                        {
                            step = savedInstanceState.getParcelable("step");
                        }
                    recipe= getIntent().getParcelableExtra("recipe");
                    showStepFragment(step,mTwoPane);
                    break;

                case "stepList":
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("recipe", recipe);
                    bundle.putParcelableArrayList("theIngredients", (ArrayList<? extends Parcelable>) myIngredients);
                    bundle.putParcelableArrayList("theSteps", (ArrayList<? extends Parcelable>) mySteps);
                    if(savedInstanceState==null&!saved){
                    stepsListFragment = new StepsListFragment();
                    stepsListFragment.setArguments(bundle);
                    android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                           // .addToBackStack(null)
                            .add(R.id.detail_frame, stepsListFragment, "stepListFragment")
                            .commit();}
                      saved = true;

                    break;

                case "ingredients":
                    showIngredientFragment();
                    break;

                default:
                    break;
            }
        }
    }
    /*
    *   shows tool bar with correct label
    */
    private void setToolbar()
    {
        //sets the title on the toolbar to the recipe name
        toolbar.setTitle(recipe.getName());
        // get a reference to the toolbar/action bar
        getSupportActionBar();
        setSupportActionBar(toolbar);
        // adds the home/up/back button to the toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    // creates the options menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Use AppCompatActivity's method getMenuInflater to get a handle on the menu inflater
        MenuInflater inflater = getMenuInflater();
        // Use the inflater's inflate method to inflate our menu layout to this menu
        inflater.inflate(R.menu.options, menu);
        // Return true so that the menu is displayed in the Toolbar
        return true;
    }
    // directs the menu item clicks
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // get the id of the menu item selected
        int id = item.getItemId();
        // parse the menu item for which item was clicked,
        // creates an instance of fragmentManager
        FragmentManager manager = getFragmentManager();
        switch(id)
        {
            // if the menu id was popular movies
            case R.id.add_widget:
                addWidgetIngredients(manager);
                break;
            case R.id.about:
                // creates an instance of the AboutDialogFragment
                AboutDialogFragment about = new AboutDialogFragment();

                about.show(manager, "about");
                //end
                break;
        }
        // the default behaviour
        return super.onOptionsItemSelected(item);
    }
    // sends the recipe and ingredients to the Widget Dialog Fragment
    private void addWidgetIngredients(FragmentManager manager)
    {
        Bundle args = new Bundle();
        args.putParcelable("recipe",recipe);
        args.putParcelableArrayList("theIngredients", (ArrayList<? extends Parcelable>) myIngredients);
        WidgetDialogFragment widget = new WidgetDialogFragment();
        widget.setArguments(args);
        //asks to show the add widget object, tells the manager its tagged as widget
        widget.show(manager, "widget");
    }
    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        outState.putParcelable("recipe",recipe);
        outState.putParcelable("step",step);
        outState.putParcelableArrayList("theIngredients", (ArrayList<? extends Parcelable>) myIngredients);
        outState.putParcelableArrayList("theSteps", (ArrayList<? extends Parcelable>) mySteps);
        super.onSaveInstanceState(outState);
    }

    //
    private void showIngredientFragment()
    {
        StepsListFragment myDetailFragment = new StepsListFragment();
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.detail_frame,myDetailFragment)
                .commit();
    }
    private void showStepFragment(Step step, Boolean mTwoPane) {

        Bundle myBundle;
        StepFragment myStepFragment;
        if (!saved)
        {
            int myStepNumber = step.getId();
            Log.v("DetailActivity", "showStepFragment, !Saved myStepNumber = " + step.getId());
            setToolbar();
            myBundle = new Bundle();
            myBundle.putParcelable("recipe", recipe);
            myBundle.putParcelableArrayList("theSteps", (ArrayList<? extends Parcelable>) mySteps);
            myBundle.putInt("step_number", myStepNumber);
            myStepFragment = new StepFragment();
            myStepFragment.setArguments(myBundle);
            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();

            Log.v("DetailActivity", "showStepFragment, !Saved ,  ");
            if(mTwoPane)
            {
                Log.v("DetailActivity", "showStepFragment, !Saved , two pane  ");
                fragmentManager.beginTransaction()
                        //.addToBackStack(null)
                        .replace(R.id.detail_second_frame, myStepFragment)
                        .commit();

            }
            else
                {
                    Log.v("DetailActivity", "showStepFragment, !Saved , single pane  ");
                    fragmentManager.beginTransaction()
                          //.addToBackStack(null)
                            .replace(R.id.detail_frame, myStepFragment)
                            .commit();
                }
            saved = true;
        }
            else
            {

                int myStepNumber = step.getId();
                Log.v("DetailActivity", "showStepFragment, !Saved myStepNumber = " + step.getId());
                setToolbar();
                myBundle = new Bundle();
                myBundle.putParcelable("recipe", recipe);
                myBundle.putParcelableArrayList("theSteps", (ArrayList<? extends Parcelable>) mySteps);
                myBundle.putInt("step_number", myStepNumber);
                 myStepFragment = new StepFragment();
                myStepFragment.setArguments(myBundle);
                android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();

                Log.v("DetailActivity", "showStepFragment, !Saved , Else = ");
                if(mTwoPane)
                {
                    fragmentManager.beginTransaction()
                          //.addToBackStack(null)
                            .replace(R.id.detail_second_frame, myStepFragment)
                            .commit();
                }
                else
                    {
                        fragmentManager.beginTransaction()
                                .addToBackStack(null)
                                .replace(R.id.detail_frame, myStepFragment)
                                .commit();
                    }
            }
    }
    @Override
    protected void onResume()
    {
        super.onResume();
        setToolbar();
    }

    // the listener for the button selection in the StepFragment
    @Override
    public void onButtonChoice(int choice)
    {
      //  saved = false;
        int stepNumber = choice;
        Log.v("DetailActivity","*************  onButtonChoice, the Button number =" + choice);
        step = mySteps.get(stepNumber);
        showStepFragment(step,mTwoPane);
    }
}
