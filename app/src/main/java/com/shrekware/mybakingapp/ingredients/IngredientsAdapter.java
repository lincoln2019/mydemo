/*
 *
 *   PROJECT LICENSE
 *   This project was submitted by Henry Ayers as part of the Nanodegree At Udacity.
 *   As part of Udacity Honor code, your submissions must be your own work,
 *   hence submitting this project as yours will cause you to break the Udacity Honor Code
 *   and the suspension of your account. Me, the author of the project,
 *   allow you to check the code as a reference,
 *   but if you submit it, it's your own responsibility if you get expelled.
 *
 *   Copyright (c) 2018 Henry Ayers
 *
 *   Besides the above notice, the following license applies and
 *   this license notice must be included in all works derived from this project.
 *   MIT License Permission is hereby granted, free of charge,
 *   to any person obtaining a copy of this software and associated documentation files (the "Software"),
 *   to deal in the Software without restriction, including without limitation the rights to use,
 *   copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 *   and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 *   DAMAGES OR OTHER LIABILITY, WHETHER ACTION OF CONTRACT,
 *   TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package com.shrekware.mybakingapp.ingredients;

import android.content.Context;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.shrekware.mybakingapp.R;

public class IngredientsAdapter extends RecyclerView.Adapter<IngredientsAdapter.ViewHolder>
{
    //declare a cursor to hold the list of ingredients
    private Cursor theList;

    public IngredientsAdapter(Cursor cursor)
    {
        // initialize the list
        theList=cursor;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @NonNull
    @Override
    public IngredientsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // declare and initialize a context
        Context myContext = parent.getContext();
        // declare and initialize an inflater
        LayoutInflater inflater = LayoutInflater.from(myContext);
        // declare and show the layout with the list
        View view = inflater.inflate(R.layout.ingredient_list_item, parent, false);
        //return a ingredients view holder view
        return new IngredientsAdapter.ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull IngredientsAdapter.ViewHolder holder, int position) {
        TextView ingredientTextView =  holder.itemView.findViewById(R.id.ingredient_item_name_tv);
        TextView quantityTextView = holder.itemView.findViewById(R.id.ingredient_item_quantity_tv);
        TextView measureTextView = holder.itemView.findViewById(R.id.ingredient_item_measure_tv);
        theList.moveToPosition(position);
        ingredientTextView.setText(theList.getString(4));
        quantityTextView.setText(theList.getString(2));
        measureTextView.setText(theList.getString(3));
    }

    @Override
    public int getItemCount() {
        // checks if the cursor is null or not
        if (theList == null)return 0;
        // if its not null, return the size
        Log.v("IngredientsAdapter", "the count"+ theList.getCount());
        return theList.getCount();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ViewHolder(View itemView) {
            super(itemView);
        }
    }


}
