/*
 *
 *   PROJECT LICENSE
 *   This project was submitted by Henry Ayers as part of the Nanodegree At Udacity.
 *   As part of Udacity Honor code, your submissions must be your own work,
 *   hence submitting this project as yours will cause you to break the Udacity Honor Code
 *   and the suspension of your account. Me, the author of the project,
 *   allow you to check the code as a reference,
 *   but if you submit it, it's your own responsibility if you get expelled.
 *
 *   Copyright (c) 2018 Henry Ayers
 *
 *   Besides the above notice, the following license applies and
 *   this license notice must be included in all works derived from this project.
 *   MIT License Permission is hereby granted, free of charge,
 *   to any person obtaining a copy of this software and associated documentation files (the "Software"),
 *   to deal in the Software without restriction, including without limitation the rights to use,
 *   copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 *   and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 *   DAMAGES OR OTHER LIABILITY, WHETHER ACTION OF CONTRACT,
 *   TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package com.shrekware.mybakingapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.appwidget.AppWidgetManager;
import android.arch.persistence.room.Room;
import android.content.ComponentName;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.shrekware.mybakingapp.database.WidgetDatabase;
import com.shrekware.mybakingapp.database.WidgetIngredient;
import com.shrekware.mybakingapp.retrofit.Ingredient;
import com.shrekware.mybakingapp.retrofit.Recipe;
import java.util.ArrayList;
import java.util.List;

public class WidgetDialogFragment extends DialogFragment
{

    private WidgetDatabase myDb;
    // private LiveData<List<WidgetIngredient>> myListOfIngredients;
    //declare a recipe variable, used to hold the recipe from the detail activity
    private Recipe recipe;
    // declares a list of Ingredient Objects
    private ArrayList<Ingredient> ingredients;
    // holds the recipe/list of recipes for the Dialog Fragment
    private ArrayList<Recipe> list;
    // declare a database to hold the list widgetIngredient Objects
//    // declare a WidgetIngredient Object
    private WidgetIngredient widgetIngredient;
    private List<WidgetIngredient> widgetIngredientList;
    // over rides on Saved Instance State, adds the recipe and a list of ingredients
    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        // adds the recipe to the saved bundle
        outState.putParcelable("recipe",recipe);
        // adds the list of ingredients to the saved bundle
        outState.putParcelableArrayList("ingredient", ingredients);

    }
    // creates the dialog view
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState)
    {
        // returns the inflated layout for the dialog
        return inflater.inflate(R.layout.widget_dialog_list,null, false);
    }
    // creates the dialog fragment
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
     //   mDb = WidgetDatabase.getInstance(getActivity().getApplicationContext());
        myDb = Room.databaseBuilder(getActivity().getApplicationContext(), WidgetDatabase.class, "widget_ingredients").build();
        Log.v("WidgetDialogFragment","**** onCreateDialog  ******************* ");

        if(savedInstanceState!=null)
        {
            recipe = savedInstanceState.getParcelable("recipe");
            ingredients = savedInstanceState.getParcelableArrayList("ingredient");
        }
        else
            {
                ingredients = new ArrayList<>();
                Log.v("WidgetDialogFragment", "*************** onCreateDialog ELSE, myList **********");
                // missing args
                //TODO missing args??
              Bundle args = getArguments();
              recipe = args.getParcelable("recipe");
              ingredients = args.getParcelableArrayList("theIngredients");
              Log.v("WidgetDialogFragment", "*********  onCreateDialog ingredient zero "+ ingredients.get(0).getIngredient());
            }
        LayoutInflater imageInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = imageInflater.inflate(R.layout.widget_dialog_list, null);
        // adds the message to the alert window
        // Use the Builder class for convenient dialog construction, adds a theme number
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), 5);
        // get an instance of layout inflater
        final ListView myList =  view.findViewById(R.id.widget_dialog_list);
        list = new ArrayList<>();
        list.add(recipe);
        WidgetAdapter widgetAdapter = new WidgetAdapter(this.getActivity(), R.layout.widget_dialog_item,list );
        myList.setAdapter(widgetAdapter);
        final String title = recipe.getName() + " Ingredients";
         builder.setView(view)
                .setTitle(title)
                .setPositiveButton("add", (dialog, id) -> {
                    // declares and initializes a string to hold the recipe name
                    String recipeName = recipe.getName();
                    Float quantity;
                    String ingredient;
                    String measure;
                    widgetIngredientList = new ArrayList<>();
                    // ingredients are a list of ingredient objects
                    for(int x=0;x<ingredients.size();x++)
                    {
                        quantity = ingredients.get(x).getQuantity();
                        ingredient = ingredients.get(x).getIngredient();
                        measure = ingredients.get(x).getMeasure();
                        Log.v("WidgetDialogFragment", " ingredient" + x + " is " + quantity + " " + measure + " " + ingredient);
                        widgetIngredient = new WidgetIngredient(recipeName, quantity, ingredient, measure);
                        widgetIngredientList.add(widgetIngredient);
                        //  stringIngredients.add(quantity+ingredient+measure);
                    }
                    Log.v("WidgetDialogFragment"," widgetIngredient ingredient = "+widgetIngredient.getIngredient());
                    getList();

                    updateMyWidget();
                })
                .setNegativeButton("remove", (dialog, which) -> {
                        Log.v("WidgetDialogFragment","you clicked remove ingredients ");

                    String theName = recipe.getName();
                    Log.v("WidgetDialogFragment","you clicked remove ingredients, recipe name is = "+ theName);

                       Thread thread = new Thread(() -> myDb.widgetDao().deleteRecipe(theName)) ;
                        thread.start();
                        while (thread.isAlive())


                   updateMyWidget();
                });
        return builder.create();
    }
    private void updateMyWidget()
    {
        Log.v("WidgetDialogFragment", "updateMyWidget " + "made it to the widget*****************************");
        Context context = getActivity().getApplicationContext();
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        ComponentName thisWidget = new ComponentName(context, NewAppWidget.class);
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
        AppWidgetManager.getInstance(context).notifyAppWidgetViewDataChanged(appWidgetIds,R.id.widget_list);
    }

    // adds the list of ingredients to the database
    private void getList()
    {
        Thread thread = new Thread(() -> {
            long[] mylong =  myDb.widgetDao().addAll(widgetIngredientList);
            Log.v("WidgetDialogFragment", "mylong object 0 = " + mylong[0]);
        });
        thread.start();
        while (thread.isAlive())
            Log.v("WidgetDialogFragment", "widget ingredients size = " + ingredients.size());
    }
}

