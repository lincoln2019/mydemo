/*
 *
 *   PROJECT LICENSE
 *   This project was submitted by Henry Ayers as part of the Nanodegree At Udacity.
 *   As part of Udacity Honor code, your submissions must be your own work,
 *   hence submitting this project as yours will cause you to break the Udacity Honor Code
 *   and the suspension of your account. Me, the author of the project,
 *   allow you to check the code as a reference,
 *   but if you submit it, it's your own responsibility if you get expelled.
 *
 *   Copyright (c) 2018 Henry Ayers
 *
 *   Besides the above notice, the following license applies and
 *   this license notice must be included in all works derived from this project.
 *   MIT License Permission is hereby granted, free of charge,
 *   to any person obtaining a copy of this software and associated documentation files (the "Software"),
 *   to deal in the Software without restriction, including without limitation the rights to use,
 *   copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 *   and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 *   DAMAGES OR OTHER LIABILITY, WHETHER ACTION OF CONTRACT,
 *   TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package com.shrekware.mybakingapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.shrekware.mybakingapp.R;
import com.shrekware.mybakingapp.retrofit.Recipe;

import java.util.List;

class WidgetAdapter extends ArrayAdapter<Recipe> implements View.OnClickListener {
    private List<Recipe> recipeList;
    private Context myContext;
    public WidgetAdapter(@NonNull Context context,int resource, List<Recipe> list) {
        super(context, resource );
        myContext = context;
        recipeList = list;

    }
    // returns the size of the list for the adapter
    @Override
    public int getCount() {

        if(recipeList==null)return 0;
        return recipeList.size();
    }
    //  if you dont override with a return 1, the widget list will not populate the first time you load the widget!!!
    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        Log.v("WidgetAdapter", "*************** getView **********");
        LayoutInflater inflater = LayoutInflater.from(myContext);
        View view = inflater.inflate(R.layout.widget_dialog_item,parent,false);
        TextView textView = view.findViewById(R.id.widget_dialog_textView);
        String recipeWidget = "Would you like to add the ingredients for "+ recipeList.get(0).getName()+" to the Widget?";
        textView.setText(recipeWidget);
        view.setTag(position);
        return view;
    }

    @Override
    public void onClick(View v) {
      //  Object position = v.getTag();
      //  Log.v("WidgetAdapter", "the on click in checkbox" + position.toString());

    }
}

