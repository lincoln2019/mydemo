package com.shrekware.mybakingapp;


import android.app.Activity;
import android.app.FragmentManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.shrekware.mybakingapp.database.IngredientDataBaseHelper;
import com.shrekware.mybakingapp.database.IngredientsContract;
import com.shrekware.mybakingapp.retrofit.Ingredient;
import com.shrekware.mybakingapp.retrofit.Recipe;
import com.shrekware.mybakingapp.retrofit.RetrofitClient;
import com.shrekware.mybakingapp.retrofit.Step;
import java.io.Serializable;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements RecipeListAdapter.RecipeListAdapterOnClickHandler
{
    private String LOG_TAG = "MainActivity";
    // app context
    private Context mContext;
    // the results of the retrofit call for the recipes
    private android.support.v4.app.FragmentManager fragmentManager;
    private List<Recipe> myList;
    private List<Ingredient> myIngredients;
    private LinearLayoutManager myLinearLayout;
    private GridLayoutManager myGridLayoutManager;
    // stores the recipe viewed position
    private int position;
    //used for onAttach
    private Activity activity;
    @BindView(R.id.recipeRecyclerView)
    RecyclerView recipeRecyclerView;
    @BindView(R.id.main_activity_progressBar)
    ProgressBar progressBar;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // saves the position of the first viewable recipe
        // in order to restore to this position
        outState.putInt("position", myGridLayoutManager.findFirstVisibleItemPosition());
        super.onSaveInstanceState(outState);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // sets the view to the activity main layout
        setContentView(R.layout.activity_main);
        // calculates the number of columns for the gridView
        int columns = calculateNoOfColumns(getApplicationContext());
        // gets the last viewed position if available
        if(savedInstanceState!= null)
                {position = savedInstanceState.getInt("position"); }
                else
                    {position= 0;}
         // gets an instance of the fragment manager
        fragmentManager = getSupportFragmentManager();
        // recipeList adapter and adds a clickHandler
        RecipeListAdapter recipeAdapter = new RecipeListAdapter(myList,getClickHandler());
        mContext = this;

        IngredientDataBaseHelper myHelper = new IngredientDataBaseHelper(mContext);
        final SQLiteDatabase sqlDB =  myHelper.getWritableDatabase();
        // to clear the table, otherwise the recycler view gets very long/repeating
        sqlDB.execSQL("delete from "+ IngredientsContract.Ingredients.cTABLE_NAME);
        // binds butterKnife
        ButterKnife.bind(this);
        //  creates and instantiates a grid layout based on number of columns
        myGridLayoutManager = new GridLayoutManager(this, columns);
        // binds the recyclerView to the gridLayout
        recipeRecyclerView.setLayoutManager(myGridLayoutManager);
        // adds the recipe adapter to the recyclerView
        recipeRecyclerView.setAdapter(recipeAdapter);
        // gets the list of Recipes
        getRecipesJson();
    }
    private void getRecipesJson()
    {
        // shows the loading indicator
         showLoading();
        // checks if the list of movie object exists
        // gets an instance of retrofit client
        RetrofitClient client = new RetrofitClient();
        // checks if internet/network status is available
        if (getInternetStatus()) {
            Log.v(LOG_TAG,"get internet status");

            // the retrofit call to retrieve the Recipes List and associated list of objects
            client.getApiService().getRecipes().enqueue(new Callback<List<Recipe>>()
            {
                // the response of the retrofit call
                @Override
                public void onResponse(@NonNull Call<List<Recipe>> call, @NonNull Response<List<Recipe>> response)
                {
                    // checks if the response is null
                    if (response.body() != null)
                    {
                        //hides progressbar, shows list
                        doneLoading();

                        // loads the response into the resultsList
                        myList = response.body();
                    }
                    // set recycler view to a linear layout
                    recipeRecyclerView.setLayoutManager(myGridLayoutManager);
                    // sets the recycler view data to the recipe list, myList
                    recipeRecyclerView.setAdapter(new RecipeListAdapter(myList,getClickHandler()));
                    // sets the the adapter position to the last viewed position if any
                    recipeRecyclerView.getLayoutManager().scrollToPosition(position);
                }
                // if the call to theMovieDb should fail
                @Override
                public void onFailure(@NonNull Call<List<Recipe>> call, @NonNull Throwable t)
                {
                    // logs the error message of a failure
                    Log.d(LOG_TAG, getString(R.string.log_error_message_retrofit_call) + t.toString());
                }
            });
        }
        else
            {   // if no internet connection
                //set recyclerView to the empty list
                recipeRecyclerView.setAdapter(new RecipeListAdapter(myList,getClickHandler()));
                //  we close the loading indicator and shows the recyclerView
                // shows toast stating No Internet Connection
                Toast.makeText(this, R.string.toast_no_internet_access,Toast.LENGTH_LONG).show();
            }
    }
    // shows loading indicator
   private void showLoading()
   {
        // show the loading indicator
        progressBar.setVisibility(View.VISIBLE);
        // hide the movie list
        recipeRecyclerView.setVisibility(View.GONE);
    }
    private void doneLoading()
    {
        // show the loading indicator
        progressBar.setVisibility(View.GONE);
        // hide the movie list
        recipeRecyclerView.setVisibility(View.VISIBLE);
    }
    @Override
    public void onAttach(Activity activity)
    {
        this.activity = activity;
    }

    //checks the device to see if there is a network connection
    private boolean getInternetStatus()
    {
        // opens a dialog to the phone about its connection to the network
        ConnectivityManager connectivityManager =
                (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        // gets an instance of the NetworkInfo.class
        NetworkInfo networkInfo = null;
        // checks to see if the connectivity manager is available
        if (connectivityManager != null) {
            //asks the phone if it has a network connection
            networkInfo = connectivityManager.getActiveNetworkInfo();
        }
        // returns true  if network information is not null and the network is connected
        // does not test internet access, it could be blocked!, but not likely...
        return (networkInfo != null && networkInfo.isConnected());
    }
    @Override
    public void onClick(Recipe recipe)
    {
        Toast.makeText(this,"you clicked "+recipe.getName(),Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, DetailActivity.class);
        List<Step> mySteps = recipe.getSteps();
        myIngredients = recipe.getIngredients();
        intent.putExtra("theIngredients", (Serializable) myIngredients);
        intent.putExtra("theSteps", (Serializable) mySteps);
        intent.putExtra("recipe",recipe);
        intent.putExtra("step",mySteps.get(0));
        // adds ingredients to the sqLite database
        addIngredients(recipe);
        // opens the Detail Activity
        startActivity(intent);
    }
    private void addIngredients(Recipe recipe){
        String myRecipeName = recipe.getName();
        // to clear the table, otherwise the recycler view gets very long/repeating
        IngredientDataBaseHelper myHelper = new IngredientDataBaseHelper(mContext);
        final SQLiteDatabase sqlDB =  myHelper.getWritableDatabase();
        sqlDB.execSQL("delete from "+ IngredientsContract.Ingredients.cTABLE_NAME);
        // initializes a content values object to store the values for the table columns
        ContentValues myContent = new ContentValues();
        myContent.put(IngredientsContract.Ingredients.cRECIPE,myRecipeName);
        List<Ingredient> myList = recipe.getIngredients();
        for(int x= 0; x<myList.size();x++)
        {
            // add the ingredient to the content values
            myContent.put(IngredientsContract.Ingredients.cINGREDIENT,myList.get(x).getIngredient());
            // adds the measure amount to the content values
            myContent.put(IngredientsContract.Ingredients.cMEASURE,myList.get(x).getMeasure());
            // adds the quantity to the content values
            myContent.put(IngredientsContract.Ingredients.cQUANTITY, myList.get(x).getQuantity());
            // retrieves a local reference to the favorites db Uri
            Uri uri = IngredientsContract.Ingredients.CONTENT_URI;
            // insert a new Ingredient list into the favorites db, could retrieve the uri of the insert
            getContentResolver().insert(uri,myContent);
        }
        sqlDB.close();
    }
    private RecipeListAdapter.RecipeListAdapterOnClickHandler getClickHandler(){
        return this;
    }
    // creates the options menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Use AppCompatActivity's method getMenuInflater to get a handle on the menu inflater
        MenuInflater inflater = getMenuInflater();
        // Use the inflater's inflate method to inflate our menu layout to this menu
        inflater.inflate(R.menu.main_menu, menu);
        // Return true so that the menu is displayed in the Toolbar
        return true;
    }
    // directs the menu item clicks
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // get the id of the menu item selected
        int id = item.getItemId();
        // parse the menu item for which item was clicked,
        // creates an instance of fragmentManager
        switch(id)
        {

            case R.id.add_widget:

                break;

            case R.id.about:
                // creates an instance of the AboutDialogFragment
                AboutDialogFragment about = new AboutDialogFragment();
                // creates an instance of fragmentManager
             FragmentManager manager = getFragmentManager();
                //asks to show the about object, tells the manager its tagged as about
                about.show(manager,"about");
                //returns done
                break;
        }
        // the default behaviour
        return super.onOptionsItemSelected(item);
    }
    private static int calculateNoOfColumns(Context context)
    {
        // creates and initializes a display variable
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        // creates a float to hold how wide the phone screen is
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        // constant for the column scale size
        int scalingFactor = 600;
        // figures out how many columns at 200dp can fit on the phone
        int noOfColumns = (int) (dpWidth / scalingFactor);
        // if there is not enough room for 2, show 2 anyway
       if(noOfColumns < 1)
            // sets columns to 1
           noOfColumns = 1;
        // returns the number of columns to display
        return noOfColumns;
    }
}
