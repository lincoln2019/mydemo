/*
 *
 *   PROJECT LICENSE
 *   This project was submitted by Henry Ayers as part of the Nanodegree At Udacity.
 *   As part of Udacity Honor code, your submissions must be your own work,
 *   hence submitting this project as yours will cause you to break the Udacity Honor Code
 *   and the suspension of your account. Me, the author of the project,
 *   allow you to check the code as a reference,
 *   but if you submit it, it's your own responsibility if you get expelled.
 *
 *   Copyright (c) 2018 Henry Ayers
 *
 *   Besides the above notice, the following license applies and
 *   this license notice must be included in all works derived from this project.
 *   MIT License Permission is hereby granted, free of charge,
 *   to any person obtaining a copy of this software and associated documentation files (the "Software"),
 *   to deal in the Software without restriction, including without limitation the rights to use,
 *   copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 *   and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 *   DAMAGES OR OTHER LIABILITY, WHETHER ACTION OF CONTRACT,
 *   TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package com.shrekware.mybakingapp;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.shrekware.mybakingapp.retrofit.Recipe;
import java.util.List;

public class RecipeListAdapter extends RecyclerView.Adapter<RecipeListAdapter.ViewHolder> {
    final private List<Recipe> theRecipeList;
    private View view;
    private  Context context;
    final private RecipeListAdapterOnClickHandler myClicker;
    public RecipeListAdapter(List<Recipe> recipeList, RecipeListAdapterOnClickHandler clickHandler)
    {
        myClicker = clickHandler;
        theRecipeList = recipeList;
    }

    public interface RecipeListAdapterOnClickHandler
    {
        // shows loading indicator
    /*    private void showLoading() {
            // show the loading indicator
            mProgressBar.setVisibility(View.VISIBLE);
            // hide the movie list
            movieListRecyclerView.setVisibility(View.GONE);
        }*/
        void onAttach(Activity activity);

        void onClick(Recipe recipe);
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.recipe_item_layout, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position)
    {
        TextView recipeTextView = holder.itemView.findViewById(R.id.recipe_tv);
        recipeTextView.setText(theRecipeList.get(position).getName());
        Log.v("RecipeListAdapter","image ***** "+ theRecipeList.get(position).getImage());
        Log.v("RecipeListAdapter","name ***** "+ theRecipeList.get(position).getName());
        switch(theRecipeList.get(position).getName()){
            case "Nutella Pie":
                Log.v("RecipeListAdapter","image ***** "+ theRecipeList.get(position).getImage());

                Drawable myPic = context.getResources().getDrawable(R.drawable.coming_soon);
                recipeTextView.setBackground(myPic);
                recipeTextView.setTextColor(Color.BLACK);
                break;
            case "Brownies":
                Log.v("RecipeListAdapter","image ***** "+ theRecipeList.get(position).getImage());

                myPic =   context.getResources().getDrawable (R.drawable.coming_soon);
                recipeTextView.setBackground(myPic);
                recipeTextView.setTextColor(Color.BLACK);

                break;
            case "Yellow Cake":
                Log.v("RecipeListAdapter","image ***** "+ theRecipeList.get(position).getImage());
                myPic = context.getResources().getDrawable(R.drawable.coming_soon);
                recipeTextView.setBackground(myPic);
                recipeTextView.setTextColor(Color.BLACK);
                break;

            case "Cheesecake":
                Log.v("RecipeListAdapter","image ***** "+ theRecipeList.get(position).getImage());

                myPic =   context.getResources().getDrawable(R.drawable.coming_soon);
                recipeTextView.setBackground(myPic);
                recipeTextView.setTextColor(Color.BLACK);
                break;
            default:
                myPic =  context.getResources().getDrawable(R.drawable.coming_soon);
                recipeTextView.setBackground(myPic);
                recipeTextView.setTextColor(Color.BLACK);
                break;
        }
    }
    @Override
    public int getItemCount() {
        if(theRecipeList==null)return 0;
        return theRecipeList.size();
    }
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
    }
        @Override
        public void onClick(View v) {
            Recipe myRecipe = theRecipeList.get(getAdapterPosition());
            myClicker.onClick(myRecipe);
            Log.v("adapter onClick", "the name is " + myRecipe.getName());
        }
    }
}
