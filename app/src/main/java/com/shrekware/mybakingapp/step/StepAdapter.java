/*
 *
 *   PROJECT LICENSE
 *   This project was submitted by Henry Ayers as part of the Nanodegree At Udacity.
 *   As part of Udacity Honor code, your submissions must be your own work,
 *   hence submitting this project as yours will cause you to break the Udacity Honor Code
 *   and the suspension of your account. Me, the author of the project,
 *   allow you to check the code as a reference,
 *   but if you submit it, it's your own responsibility if you get expelled.
 *
 *   Copyright (c) 2018 Henry Ayers
 *
 *   Besides the above notice, the following license applies and
 *   this license notice must be included in all works derived from this project.
 *   MIT License Permission is hereby granted, free of charge,
 *   to any person obtaining a copy of this software and associated documentation files (the "Software"),
 *   to deal in the Software without restriction, including without limitation the rights to use,
 *   copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 *   and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 *   DAMAGES OR OTHER LIABILITY, WHETHER ACTION OF CONTRACT,
 *   TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package com.shrekware.mybakingapp.step;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.shrekware.mybakingapp.R;
import com.shrekware.mybakingapp.retrofit.Step;
import java.util.List;

public class StepAdapter extends RecyclerView.Adapter<StepAdapter.ViewHolder>
{
    private View view;
    private final List<Step> mySteps;
    private StepAdapter.StepsAdapterOnClickHandler myClickHandler;
    private TextView stepTextView;
    public StepAdapter(List<Step> stepList, StepAdapter.StepsAdapterOnClickHandler clickHandler)
    {
        mySteps = stepList;
        myClickHandler = clickHandler;
    }
    @NonNull
    @Override
    public StepAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        view = inflater.inflate(R.layout.step_item_layout,parent,false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull StepAdapter.ViewHolder holder, int position) {
        stepTextView = holder.itemView.findViewById(R.id.step_tv);
        String stepText = mySteps.get(position).getShortDescription();
        Log.v("Steps Adapter ******", "short description : "+ stepText);
        stepTextView.setText(stepText);
    }
    @Override
    public int getItemCount() {
        Log.v("StepAdapter", "size of steps is null");
        if(mySteps == null)return 0;
        Log.v("StepAdapter", "size of steps"+mySteps.size());
        return mySteps.size();
    }
    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ViewHolder(View itemView) {


            super(itemView);
            view.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {

            Log.v("adapter onClick", "the name is ");
            //on the click of a recyclerView item, we retrieve the
            //step object in the view clicked
            Step myStep = mySteps.get(getAdapterPosition());

            // pass the step object to the instance of the MovieListAdapterOnClickHandler
            myClickHandler.onClick(myStep);
        }
    }






    public interface StepsAdapterOnClickHandler
    {
        void onClick(Step step);
    }


}