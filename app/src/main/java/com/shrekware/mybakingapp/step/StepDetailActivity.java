/*
 *
 *   PROJECT LICENSE
 *   This project was submitted by Henry Ayers as part of the Nanodegree At Udacity.
 *   As part of Udacity Honor code, your submissions must be your own work,
 *   hence submitting this project as yours will cause you to break the Udacity Honor Code
 *   and the suspension of your account. Me, the author of the project,
 *   allow you to check the code as a reference,
 *   but if you submit it, it's your own responsibility if you get expelled.
 *
 *   Copyright (c) 2018 Henry Ayers
 *
 *   Besides the above notice, the following license applies and
 *   this license notice must be included in all works derived from this project.
 *   MIT License Permission is hereby granted, free of charge,
 *   to any person obtaining a copy of this software and associated documentation files (the "Software"),
 *   to deal in the Software without restriction, including without limitation the rights to use,
 *   copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 *   and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 *   DAMAGES OR OTHER LIABILITY, WHETHER ACTION OF CONTRACT,
 *   TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package com.shrekware.mybakingapp.step;

import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.shrekware.mybakingapp.R;
import com.shrekware.mybakingapp.retrofit.Step;

import java.util.ArrayList;
import java.util.List;

public class StepDetailActivity extends AppCompatActivity {

    private List<Step> listOfSteps;
    private int myStepNumber;
    private StepFragment myStepFragment;
    private  Bundle myBundle;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_step_detail);
    Intent intent = getIntent();
    listOfSteps = intent.getParcelableArrayListExtra("theSteps");
    myStepNumber = intent.getIntExtra("step_number", 0);

     myBundle = new Bundle();
    myBundle.putParcelableArrayList("theSteps", (ArrayList<? extends Parcelable>) listOfSteps);
    myBundle.putInt("step_number", myStepNumber);


            myStepFragment = new StepFragment();
            myStepFragment.setArguments(myBundle);
            android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .addToBackStack(null)
                    .replace(R.id.detail_frame,myStepFragment)
                    .commit();




    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }
}
