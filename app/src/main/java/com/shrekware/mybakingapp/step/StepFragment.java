package com.shrekware.mybakingapp.step;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.media.session.MediaSessionCompat;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.TextView;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.LoadControl;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.shrekware.mybakingapp.R;
import com.shrekware.mybakingapp.retrofit.Recipe;
import com.shrekware.mybakingapp.retrofit.Step;

import java.util.ArrayList;
import java.util.List;

public class StepFragment extends Fragment implements Player.EventListener{
    private TextView shortDescription;
    private TextView myStepText;
    private List<Step> listOfSteps;
    private int myStepNumber;
    private Button nextButton;
    private Button previousButton;
    private Step thisStep;
    private SimpleExoPlayer mExoPlayer;
    private PlayerView mPlayerView;
    private static MediaSessionCompat mMediaSession;
    private PlaybackStateCompat.Builder mStateBuilder;
    private DataSource.Factory mediaDataSourceFactory;
    private Recipe recipe;
    private OnFragmentInteractionListener mListener;
    private long currentPosition;
    public StepFragment()
    {
        //constructor
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // listens for the step number changing
        mListener = (OnFragmentInteractionListener)getActivity();
    }
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState)
    {
        Log.v("StepFragment", "onSaveInstanceState,  my step number is = "+ myStepNumber);
        outState.putLong("currentPosition", currentPosition);
        Log.v("StepFragment", "onSaveInstanceState,  my currentPosition = "+ currentPosition);
        outState.putParcelableArrayList("theSteps", (ArrayList<? extends Parcelable>) listOfSteps);
        outState.putParcelable("theStep",thisStep);
        outState.putInt("position",myStepNumber);
        outState.putParcelable("recipe", recipe);
        super.onSaveInstanceState(outState);
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        Log.v("StepFragment", "******************onActivityCreated ");
        // retrieve the position the video was left playing at, if a video existed
    //  if(savedInstanceState!=null) currentPosition = savedInstanceState.getLong("currentPosition");
        super.onActivityCreated(savedInstanceState);
    }
    @Override
    public void onViewStateRestored(@Nullable Bundle bundle)
    {
        Log.v("StepFragment", "******************  onViewStateRestored ");
        //if there is a bundle, then it was restored
        if(bundle!=null)
        {
            thisStep = bundle.getParcelable("theStep");
            myStepNumber = bundle.getInt("position");
            recipe = bundle.getParcelable("recipe");
            currentPosition = bundle.getLong("currentPosition");
            listOfSteps = bundle.getParcelableArrayList("theSteps");
            Log.v("StepFragment", "******************  onViewStateRestored, bundle != null, currentPosition = "+ currentPosition);
        }
        Log.v("StepFragment", "******************  onViewStateRestored, currentPosition = "+ currentPosition);
        super.onViewStateRestored(bundle);
    }
    @Override
    public void onResume()    {
        // reference for this context
        Context context = getContext();
        // add the step event listener back to the fragment
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
   if(mExoPlayer==null){
        // Initialize the Media Session.
        initializeMediaSession();

        getVideo(thisStep);
    }
        // Seek to the last position of the player.
        if(mExoPlayer!=null){
        mExoPlayer.seekTo(currentPosition);}



        Log.v("StepFragment", "******************  onResume ");
        Log.v("StepFragment", "******************onResume, currentPosition = "+ currentPosition);
        super.onResume();
    }
    @Override
    public void onPause()
    {
        // get current position it we have an exoPlayer
        if(mExoPlayer!=null)
        {
            currentPosition = mExoPlayer.getCurrentPosition();
        }
        Log.v("StepFragment", "******************  onPause, current Position = "+ currentPosition);
        super.onPause();
    }

    @Override
    public void onDestroy() {
      releasePlayer();
        super.onDestroy();
    }

    @Override
    public void onStop()
    {
        Log.v("StepFragment", "******************  onStop ");
      releasePlayer();
        super.onStop();
    }
    public void checkSavedState(Bundle bundle)
    {
        if(bundle!=null)
        {
            Log.v("StepFragment", "******************  checkSavedState(bundle) not NULL");
            currentPosition=bundle.getLong("currentPosition");
            Log.v("StepFragment", "******************  checkSavedState(bundle) current Position = "+ currentPosition);
            thisStep = bundle.getParcelable("theStep");
            myStepNumber = bundle.getInt("position");
            recipe = bundle.getParcelable("recipe");
            listOfSteps = bundle.getParcelableArrayList("theSteps");
            Log.v("StepFragment", "saved state is not null");
            Log.v("StepFragment", "saved state my step number is = "+ thisStep.getDescription());
        }
        // else get the arguments from the activity
        else
            {
                recipe = getArguments().getParcelable("recipe");
                listOfSteps = getArguments().getParcelableArrayList("theSteps");
                myStepNumber = getArguments().getInt("step_number");
                thisStep = listOfSteps.get(myStepNumber);
                Log.v("StepFragment", "checkSavedState(bundle) is NULL, this is the step number "+ thisStep.getDescription());
            }
        Log.v("StepFragment", "checkSavedState(bundle) currentPosition = "+ currentPosition);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }
    @Override
    public void onStart() {

        super.onStart();

    }

    private void getVideo(Step thisStep)
    {
        // show the player
        mPlayerView.setVisibility(View.VISIBLE);
        // retrieve the uri from the Step
        Uri uri = Uri.parse(thisStep.getVideoURL());
        Log.v("StepFragment", "******************getVideo, getVideoURL uri = "+ uri);
        // if the video url is not valid
        if(!URLUtil.isValidUrl(thisStep.getVideoURL()))
        {
            // try the thumbnail
            uri = Uri.parse(thisStep.getThumbnailURL());
            Log.v("StepFragment", "******************getThumbnailURL, uri = " + uri);
            // if no thumbnail
            if (!URLUtil.isValidUrl(thisStep.getThumbnailURL()))
            {
                // hide the player
                mPlayerView.setVisibility(View.GONE);
                Log.v("StepFragment", "******************getThumbnailURL, player GONE ");
                // end
                return;
            }
        }
           //else if there is a video or thumbnail url initialize the player
           // and show url
           initializePlayer(uri);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState)
    {

        if(savedInstanceState!=null)
        {
            Log.v("StepFragment", "******************  checkSavedState(bundle) not NULL");
            currentPosition=savedInstanceState.getLong("currentPosition");
            Log.v("StepFragment", "******************  checkSavedState(bundle) current Position = "+ currentPosition);
            thisStep = savedInstanceState.getParcelable("theStep");
            myStepNumber = savedInstanceState.getInt("position");
            recipe = savedInstanceState.getParcelable("recipe");
            listOfSteps = savedInstanceState.getParcelableArrayList("theSteps");
            Log.v("StepFragment", "saved state is not null");
            Log.v("StepFragment", "saved state my step number is = "+ thisStep.getDescription());
        }
        // else get the arguments from the activity
        else
        {
            recipe = getArguments().getParcelable("recipe");
            listOfSteps = getArguments().getParcelableArrayList("theSteps");
            myStepNumber = getArguments().getInt("step_number");
            thisStep = listOfSteps.get(myStepNumber);
            currentPosition=0;
            Log.v("StepFragment", "checkSavedState(bundle) is NULL, this is the step number "+ thisStep.getDescription());
        }
        Log.v("StepFragment", "******************onCreateView ");
        // inflate a view for the fragment
        View myView = inflater.inflate(R.layout.step_fragment,container,false);
        mPlayerView =  myView.findViewById(R.id.videoViewStep);
        mPlayerView.setVisibility(View.VISIBLE);

        Log.v("StepFragment", "******************onCreateView, ");
        // Initialize the player view.
        shortDescription = myView.findViewById(R.id.step_fragment_short_description_tv);
        nextButton = myView.findViewById(R.id.button_next);
        previousButton = myView.findViewById(R.id.button_previous);
        myStepText = myView.findViewById(R.id.step_fragment_tv);
        // set view of previous and next buttons
        setButtons();

        String stepText = thisStep.getDescription() + "\n thumbnail url = " + thisStep.getThumbnailURL()
                + "\n  get Video url = " + thisStep.getVideoURL();
        myStepText.setText(stepText);
        shortDescription.setText(thisStep.getShortDescription());

        // Load the question mark as the background image until the user answers the question.
        mPlayerView.setDefaultArtwork(BitmapFactory.decodeResource
                (getResources(), R.drawable.small_loading_icon));

        // when the next button is clicked
        nextButton.setOnClickListener(v ->
        {
            // adds one to the step number
            myStepNumber++;
            // gets a new reference to the step
            thisStep = listOfSteps.get(myStepNumber);
            // sends the step number to the detail activity
            mListener.onButtonChoice(myStepNumber);
        });
        //  when the previous button is clicked
        previousButton.setOnClickListener(v ->
        {
            // subtracts one from the step number
            myStepNumber--;
            // gets a reference to the new step
            thisStep = listOfSteps.get(myStepNumber);
            // sends the step number to the detail activity
            mListener.onButtonChoice(myStepNumber);
        });
        return myView;
    }
    @Override
    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {}
    @Override
    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {}
    @Override
    public void onLoadingChanged(boolean isLoading){}
    @Override
    public void onPlayerStateChanged(boolean playWhenReady, int playbackState){}
    @Override
    public void onRepeatModeChanged(int repeatMode){}
    @Override
    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled){}
    @Override
    public void onPlayerError(ExoPlaybackException error){}
    @Override
    public void onPositionDiscontinuity(int reason){}
    @Override
    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters){}
    @Override
    public void onSeekProcessed(){}
    /**
     * Media Session Callbacks, where all external clients control the player.
     */
    private class MySessionCallback extends MediaSessionCompat.Callback
    {
        @Override
        public void onPlay() {
            mExoPlayer.setPlayWhenReady(true);
        }

        @Override
        public void onPause() {
            mExoPlayer.setPlayWhenReady(false);
        }

        @Override
        public void onSkipToPrevious() {
            mExoPlayer.seekTo(0);
        }
    }
   /*
   *    shows the next and previous button when appropriate
   */
   private void setButtons()
   {
       int totalSize = 0;
        //get size of list to see if we need to hide the next button
        if(listOfSteps!=null) totalSize = listOfSteps.size()-1;
        //reset the button to viewable
        previousButton.setVisibility(View.VISIBLE);
       //reset the button to viewable
        nextButton.setVisibility(View.VISIBLE);
        // if its the first step, hide the previous button
        if(myStepNumber==0)previousButton.setVisibility(View.GONE);
        // if its the last step, hide the next button
        if(myStepNumber==totalSize)nextButton.setVisibility(View.GONE);
   }
    /**
     * Initializes the Media Session to be enabled with media buttons, transport controls, callbacks
     * and media controller.
     */
    private void initializeMediaSession()
    {
        // Create a MediaSessionCompat.
        mMediaSession = new MediaSessionCompat(getActivity().getApplicationContext(), "StepFragment");

        // Enable callbacks from MediaButtons and TransportControls.
        mMediaSession.setFlags(
                MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS |
                        MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS);

        // Do not let MediaButtons restart the player when the app is not visible.
        mMediaSession.setMediaButtonReceiver(null);

        // Set an initial PlaybackState with ACTION_PLAY, so media buttons can start the player.
        mStateBuilder = new PlaybackStateCompat.Builder()
                .setActions(
                        PlaybackStateCompat.ACTION_PLAY |
                                PlaybackStateCompat.ACTION_PAUSE |
                                PlaybackStateCompat.ACTION_SKIP_TO_PREVIOUS |
                                PlaybackStateCompat.ACTION_PLAY_PAUSE);

        mMediaSession.setPlaybackState(mStateBuilder.build());

        // MySessionCallback has methods that handle callbacks from a media controller.
        mMediaSession.setCallback(new MySessionCallback());

        // Start the Media Session since the activity is active.
        mMediaSession.setActive(true);
    }
    /**
     * Initialize ExoPlayer.
     * @param mediaUri The URI of the sample to play.
     */
    private void initializePlayer(Uri mediaUri)
    {
        if (mExoPlayer == null) {
            // Create an instance of the ExoPlayer.
            TrackSelector trackSelector = new DefaultTrackSelector();
            LoadControl loadControl = new DefaultLoadControl();
            mExoPlayer = ExoPlayerFactory.newSimpleInstance(getActivity(), trackSelector, loadControl);
            mPlayerView.setPlayer(mExoPlayer);
            // Set the ExoPlayer.EventListener to this activity.
            mExoPlayer.addListener(this);
            String userAgent = Util.getUserAgent(getActivity().getApplicationContext(), "StepFragment");
            mediaDataSourceFactory = new DefaultDataSourceFactory(getActivity().getApplicationContext(),
                    userAgent, null);
            // Prepare the MediaSource.
            mediaDataSourceFactory.createDataSource();
            MediaSource mediaSource = new ExtractorMediaSource(mediaUri, new DefaultDataSourceFactory(getActivity(), userAgent), new DefaultExtractorsFactory(), null, null);
            mExoPlayer.prepare(mediaSource);
            mExoPlayer.setPlayWhenReady(true);
        }
    }
  // release exoPlayer
    private void releasePlayer()
    {
        if(mExoPlayer!=null){
        mExoPlayer.stop();
        mExoPlayer.release();
        mExoPlayer = null;}
    }
    // Interface definition and step callback for the previous or next button
    // it sends the step number to the activity
    public interface OnFragmentInteractionListener
    {
        void onButtonChoice(int choice);
    }

}
