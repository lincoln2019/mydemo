package com.shrekware.mybakingapp.detail;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.shrekware.mybakingapp.DetailActivity;
import com.shrekware.mybakingapp.R;
import com.shrekware.mybakingapp.ingredients.IngredientsFragment;
import com.shrekware.mybakingapp.retrofit.Ingredient;
import com.shrekware.mybakingapp.retrofit.Recipe;
import com.shrekware.mybakingapp.retrofit.Step;
import com.shrekware.mybakingapp.step.StepAdapter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class StepsListFragment extends Fragment implements StepAdapter.StepsAdapterOnClickHandler
{
    private Recipe recipe;
    private List<Step> mySteps;
    private List<Ingredient> myIngredients;
    private RecyclerView stepsRecyclerView;
    private Bundle savedBundle;
    private FragmentManager fragmentManager;

    public StepsListFragment()
    {
        DetailActivity activity = new DetailActivity();
    }
    @Override
    public void onResume()
    {
        checkSavedState();
        super.onResume();
    }
    @Override
    public void onSaveInstanceState(@NonNull Bundle outState)
    {
        outState.putParcelable("recipe",recipe);
        outState.putParcelableArrayList("theIngredients", (ArrayList<? extends Parcelable>) myIngredients);
        outState.putParcelableArrayList("theSteps", (ArrayList<? extends Parcelable>) mySteps);
        super.onSaveInstanceState(outState);
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        savedBundle = savedInstanceState;
        mySteps = checkSavedState();
        fragmentManager = getFragmentManager();
        View myView = inflater.inflate(R.layout.detail_fragment,container,false);
        stepsRecyclerView = myView.findViewById(R.id.steps_rv);
        LinearLayoutManager myLinearLayout = new LinearLayoutManager(getContext());
        stepsRecyclerView.setLayoutManager(myLinearLayout);
        StepAdapter stepsAdapter = new StepAdapter(mySteps,getClickHandler());
        stepsRecyclerView.setAdapter(stepsAdapter);
        TextView ingredientsTextView = myView.findViewById(R.id.ingredients_tv);
        ingredientsTextView.setOnClickListener(v -> {
            Bundle myBundle = new Bundle();
            myBundle.putParcelableArrayList("theIngredients", (ArrayList<? extends Parcelable>) myIngredients);
            IngredientsFragment myIngredientFragment = new IngredientsFragment();
            myIngredientFragment.setArguments(myBundle);
            fragmentManager.beginTransaction()
                  .addToBackStack(null)
                  .replace(R.id.detail_frame,myIngredientFragment)
                  .commit();
        });
        return myView;
    }
    private List<Step> checkSavedState()
    {
        //if there is no saved bundle, get the info from the intent
        if(savedBundle==null)
        {
           recipe = getArguments().getParcelable("recipe");
           mySteps   = getArguments().getParcelableArrayList("theSteps");
           myIngredients = getArguments().getParcelableArrayList("theIngredients");
        }
        // if there is saved information
        else
            {
                Log.v("StepsListFragment", "saved state IS NULL");
                mySteps= savedBundle.getParcelableArrayList("theSteps");
                myIngredients = savedBundle.getParcelableArrayList("theIngredients");
                recipe= savedBundle.getParcelable("recipe");
            }
        return mySteps;
    }
    //  to reference the on clickHandler
    private StepAdapter.StepsAdapterOnClickHandler getClickHandler()
    {
        //returns this for the OnClickHandler
        return this;
    }
    @Override
    public void onClick(Step step)
    {
        Intent intent = new Intent(getContext(),DetailActivity.class);
        intent.putExtra("frag_type","step");
        intent.putExtra("step", step);
        intent.putExtra("theIngredients", (Serializable) myIngredients);
        intent.putExtra("theSteps", (Serializable) mySteps);
        intent.putExtra("recipe",recipe);
        startActivity(intent);
    }
}
