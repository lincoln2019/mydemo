/*
 *
 *   PROJECT LICENSE
 *   This project was submitted by Henry Ayers as part of the Nanodegree At Udacity.
 *   As part of Udacity Honor code, your submissions must be your own work,
 *   hence submitting this project as yours will cause you to break the Udacity Honor Code
 *   and the suspension of your account. Me, the author of the project,
 *   allow you to check the code as a reference,
 *   but if you submit it, it's your own responsibility if you get expelled.
 *
 *   Copyright (c) 2018 Henry Ayers
 *
 *   Besides the above notice, the following license applies and
 *   this license notice must be included in all works derived from this project.
 *   MIT License Permission is hereby granted, free of charge,
 *   to any person obtaining a copy of this software and associated documentation files (the "Software"),
 *   to deal in the Software without restriction, including without limitation the rights to use,
 *   copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 *   and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 *   DAMAGES OR OTHER LIABILITY, WHETHER ACTION OF CONTRACT,
 *   TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package com.shrekware.mybakingapp.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.support.annotation.NonNull;
// adds the Room annotation and the unique indices

@Entity(tableName="widget_ingredients",primaryKeys = {"recipe_name","ingredient"}, indices = {@Index(value = {"recipe_name","ingredient"},unique = true)})
public class WidgetIngredient
{
    // the auto generated id for each item
  //  @PrimaryKey(autoGenerate = true)
    private int id;
    // the recipe name the ingredients is a member of
    @NonNull
    @ColumnInfo(name = "recipe_name")
    private String recipeName;
    // the quantity of the ingredient
    private Float quantity;
    // the measure of the  ingredient
    private String measure;
    // the description of the ingredient
    @NonNull
    private String ingredient;
    // ignores this constructor for Room
    @Ignore
    public WidgetIngredient(String recipeName, Float quantity, String ingredient, String measure)
    {
        this.recipeName =recipeName;
        this.quantity = quantity;
        this.ingredient = ingredient;
        this.measure = measure;
    }
    public WidgetIngredient(int id, String recipeName, Float quantity, String ingredient, String measure)
    {
        this.id = id;
        this.recipeName =recipeName;
        this.quantity = quantity;
        this.ingredient = ingredient;
        this.measure = measure;
    }
    public String getRecipeName()
    {
        return recipeName;
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }

    public Float getQuantity() {
        return quantity;
    }

    public void setQuantity(Float quantity) {
        this.quantity = quantity;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIngredient() {
        return ingredient;
    }

    public void setIngredient(String ingredient) {
        this.ingredient = ingredient;
    }
}
