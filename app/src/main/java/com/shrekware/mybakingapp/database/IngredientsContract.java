/*
 *
 *   PROJECT LICENSE
 *   This project was submitted by Henry Ayers as part of the Nanodegree At Udacity.
 *   As part of Udacity Honor code, your submissions must be your own work,
 *   hence submitting this project as yours will cause you to break the Udacity Honor Code
 *   and the suspension of your account. Me, the author of the project,
 *   allow you to check the code as a reference,
 *   but if you submit it, it's your own responsibility if you get expelled.
 *
 *   Copyright (c) 2018 Henry Ayers
 *
 *   Besides the above notice, the following license applies and
 *   this license notice must be included in all works derived from this project.
 *   MIT License Permission is hereby granted, free of charge,
 *   to any person obtaining a copy of this software and associated documentation files (the "Software"),
 *   to deal in the Software without restriction, including without limitation the rights to use,
 *   copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 *   and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 *   DAMAGES OR OTHER LIABILITY, WHETHER ACTION OF CONTRACT,
 *   TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package com.shrekware.mybakingapp.database;

import android.net.Uri;
import android.provider.BaseColumns;

public class IngredientsContract {


    /*
     * The "Content authority" is a name for the entire content provider, similar to the
     * relationship between a domain name and its website. A convenient string to use for the
     * content authority is the package name for the app, which is guaranteed to be unique on the
     * Play Store.
     */
    private static final String CONTENT_AUTHORITY = "com.shrekware.mybakingapplication.database";
    /*
     * Use CONTENT_AUTHORITY to create the base of all URI's which apps will use to contact
     * the content provider.
     */
    private static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY+"/"+Ingredients.cTABLE_NAME);

    /*
     *  class that holds the database column names
     */
    public static final class Ingredients implements BaseColumns
    {
        /* The base CONTENT_URI used to query the Movie Database from the content provider */
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().build();
        public static final String cDATABASE_NAME = "ingredients.db";
        public static final String cTABLE_NAME = "ingredients";
        public static final String cID = "_id";
        public static final String cRECIPE = "RECIPE";
        public static final String cQUANTITY = "QUANTITY";
        public static final String cMEASURE = "MEASURE";
        public static final String cINGREDIENT = "INGREDIENT";
    }
}
