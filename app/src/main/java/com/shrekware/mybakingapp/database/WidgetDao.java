/*
 *
 *   PROJECT LICENSE
 *   This project was submitted by Henry Ayers as part of the Nanodegree At Udacity.
 *   As part of Udacity Honor code, your submissions must be your own work,
 *   hence submitting this project as yours will cause you to break the Udacity Honor Code
 *   and the suspension of your account. Me, the author of the project,
 *   allow you to check the code as a reference,
 *   but if you submit it, it's your own responsibility if you get expelled.
 *
 *   Copyright (c) 2018 Henry Ayers
 *
 *   Besides the above notice, the following license applies and
 *   this license notice must be included in all works derived from this project.
 *   MIT License Permission is hereby granted, free of charge,
 *   to any person obtaining a copy of this software and associated documentation files (the "Software"),
 *   to deal in the Software without restriction, including without limitation the rights to use,
 *   copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 *   and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *   The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *   INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 *   DAMAGES OR OTHER LIABILITY, WHETHER ACTION OF CONTRACT,
 *   TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

package com.shrekware.mybakingapp.database;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;



import java.util.List;
@Dao
public interface WidgetDao {
    @Query("SELECT * FROM widget_ingredients ORDER BY recipe_name")
    List<WidgetIngredient> loadAllIngredients();
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertIngredient(WidgetIngredient ingredient);
    @Update()
    void updateIngredient(WidgetIngredient ingredient);
    @Delete
    void deleteIngredient(WidgetIngredient ingredient);
    @Query("SELECT * FROM widget_ingredients WHERE recipe_name LIKE :name AND ingredient LIKE :myIngredient")
    List<WidgetIngredient> findRecipes(String name, String myIngredient);

    @Query("DELETE FROM widget_ingredients WHERE recipe_name LIKE :name")
    void deleteRecipe(String name);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long[] addAll(List<WidgetIngredient> list);
}

